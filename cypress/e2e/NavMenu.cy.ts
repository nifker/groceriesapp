beforeEach(() => {
  cy.visit('/')
})

describe('Test NavMenu functionality', () => {
  it('open/close Menu', () => {
    cy.get('[data-cy="nav_menu_inner"]')
    .should('have.css', 'display', 'none')
    cy.get('[data-cy="nav_menu_open"]').click()
    cy.get('[data-cy="nav_menu_inner"]')
    .should('not.have.css', 'display', 'none')
    cy.get('[data-cy="nav_menu_open"]').click()
    cy.get('[data-cy="nav_menu_inner"]')
    .should('have.css', 'display', 'none')
  })
  it('Test NavItem 1', () => {
    cy.get('[data-cy="div_ProductTypeView"]').should('exist')
    cy.get('[data-cy="nav_menu_open"]').click()
    cy.get('[data-cy="nav_item_ProductTypeView"]').click()
    cy.get('[data-cy="nav_menu_open"]').click()

    cy.get('[data-cy="div_ProductTypeView"]').should('exist')
  })
  it('Test NavItem 2', () => {
    cy.get('[data-cy="div_ProductTypeView"]').should('exist')
    cy.get('[data-cy="nav_menu_open"]').click()
    cy.get('[data-cy="nav_item_ProductView"]').click()
    cy.get('[data-cy="nav_menu_open"]').click()

    cy.get('[data-cy="div_ProductView"]').should('exist')
  })
  it('Test NavItem 3', () => {
    cy.get('[data-cy="div_ProductTypeView"]').should('exist')
    cy.get('[data-cy="nav_menu_open"]').click()
    cy.get('[data-cy="nav_item_PurchaseListView"]').click()
    cy.get('[data-cy="nav_menu_open"]').click()

    cy.get('[data-cy="div_PurchaseListView"]').should('exist')
  })
})