const ELEMENTS = [{
  name: "Test1",
  brand: "Brand1",
  desc: "Desc1",
  ingredients: [],
  price: 2.2,
  inventory_target: 1
},
{
  name: "Test2",
  brand: "Brand2",
  desc: "Desc2",
  ingredients: ["Salt"],
  price: 3.1,
  inventory_target: 2
}
];

const PRODUCTS=[{
  type: 0,
  due_to: '2023-04-12',
  storage_location: 'Refrigerator'
},
{
  type: 1,
  due_to: '2023-06-30',
  storage_location: 'Basement'
},
{
  type: 1,
  due_to: '2023-07-12',
  storage_location: 'Basement'
}];

beforeEach(() => {
  cy.visit('/')

  for(var i=0;i<ELEMENTS.length;i++){
    cy.get('[data-cy="input_new"]').type(ELEMENTS[i].name)
    cy.get('[data-cy="button_new"]').click()

    cy.get('[data-cy="button_' + i + '_edit"').click()
    cy.get('[data-cy="input_desc"]').type(ELEMENTS[i].desc)
    cy.get('[data-cy="input_brand"]').type(ELEMENTS[i].brand)
    for(var j=0;j<ELEMENTS[i].ingredients.length;j++){
      cy.get('[data-cy="add_ingredient"').click()
      cy.get('[data-cy="input_ingredient_' + j + '"').type(ELEMENTS[i].ingredients[j])
    }
    cy.get('[data-cy="input_price"]').type(ELEMENTS[i].price.toString())
    cy.get('[data-cy="input_inventory_target"]').type(ELEMENTS[i].inventory_target.toString())

    cy.get('[data-cy="modal_update"]').click()
  }

  cy.get('[data-cy="nav_menu_open"]').click()
  cy.get('[data-cy="nav_item_ProductView"]').click()
})

describe('Test Product related functionality', () => {
  it('add/remove products', () => {
    for(var i=0;i<PRODUCTS.length;i++){
      cy.get('[data-cy="select_producttype"]').select(PRODUCTS[i].type)
      cy.get('[data-cy="date_due_to"]').type(PRODUCTS[i].due_to)
      cy.get('[data-cy="input_storage_location"]').clear().type(PRODUCTS[i].storage_location)
      cy.get('[data-cy="add_product"]').click()
    }

    //remove items
    for(var i=PRODUCTS.length-1;i>=0;i--){
      cy.get('[data-cy="item_remove_' + i + '"').click()
    }
  })
  it('test empty fields', () => {
    cy.get('[data-cy="select_producttype"]').select(PRODUCTS[0].type)
    cy.get('[data-cy="add_product"]').click()
    cy.get('[data-cy="row_item_0"]').should('not.exist')
    cy.get('[data-cy="date_due_to"]').type(PRODUCTS[0].due_to)
    cy.get('[data-cy="add_product"]').click()
    cy.get('[data-cy="row_item_0"]').should('not.exist')
    cy.get('[data-cy="input_storage_location"]').clear().type(PRODUCTS[0].storage_location)
    cy.get('[data-cy="add_product"]').click()
    cy.get('[data-cy="row_item_0"]').should('exist')
    cy.get('[data-cy="item_remove_0"]').click()
  })
  it('test sorting', () => {
    for(var i=0;i<PRODUCTS.length;i++){
      cy.get('[data-cy="select_producttype"]').select(PRODUCTS[i].type)
      cy.get('[data-cy="date_due_to"]').type(PRODUCTS[i].due_to)
      cy.get('[data-cy="input_storage_location"]').clear().type(PRODUCTS[i].storage_location)
      cy.get('[data-cy="add_product"]').click()
    }

    //test sorting
    cy.get('[data-cy="ordering_column_0"]').click()
    cy.get('[data-cy="item_type_0"]').should('contain.text', ELEMENTS[0].name);
    cy.get('[data-cy="item_type_2"]').should('contain.text', ELEMENTS[1].name);
    
    cy.get('[data-cy="ordering_column_0"]').click()
    cy.get('[data-cy="item_type_2"]').should('contain.text', ELEMENTS[0].name);
    cy.get('[data-cy="item_type_0"]').should('contain.text', ELEMENTS[1].name);
        
    cy.get('[data-cy="ordering_column_1"]').click()
    cy.get('[data-cy="item_bought_0"]').should('contain.text', new Date().toLocaleDateString());
    
    cy.get('[data-cy="ordering_column_1"]').click()
    cy.get('[data-cy="item_bought_0"]').should('contain.text', new Date().toLocaleDateString());
    
    cy.get('[data-cy="ordering_column_2"]').click()
    cy.get('[data-cy="item_due_to_0"]').should('contain.text', new Date(PRODUCTS[0].due_to).toLocaleDateString());
    cy.get('[data-cy="item_due_to_2"]').should('contain.text', new Date(PRODUCTS[2].due_to).toLocaleDateString());
    
    cy.get('[data-cy="ordering_column_2"]').click()
    cy.get('[data-cy="item_due_to_0"]').should('contain.text', new Date(PRODUCTS[2].due_to).toLocaleDateString());
    cy.get('[data-cy="item_due_to_2"]').should('contain.text', new Date(PRODUCTS[0].due_to).toLocaleDateString());
    
    cy.get('[data-cy="ordering_column_3"]').click()
    cy.get('[data-cy="item_storage_location_0"]').should('contain.text', PRODUCTS[2].storage_location);
    cy.get('[data-cy="item_storage_location_2"]').should('contain.text', PRODUCTS[0].storage_location);
    
    cy.get('[data-cy="ordering_column_3"]').click()
    cy.get('[data-cy="item_storage_location_0"]').should('contain.text', PRODUCTS[0].storage_location);
    cy.get('[data-cy="item_storage_location_2"]').should('contain.text', PRODUCTS[2].storage_location);
  })
})