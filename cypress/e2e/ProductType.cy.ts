const ELEMENTS = [{
  name: "Test1",
  brand: "Brand1",
  desc: "Desc1",
  ingredients: [],
  price: 2.2,
  inventory_target: 1
},
{
  name: "Test2",
  brand: "Brand2",
  desc: "Desc2",
  ingredients: ["Salt"],
  price: 3.1,
  inventory_target: 2
}
];

beforeEach(() => {
  cy.visit('/')
})

describe('Test ProductType related functionality', () => {
  it('add items', () => {
    for(var i=0;i<ELEMENTS.length;i++){
      cy.get('[data-cy="button_new"]').click()
      cy.get('[data-cy="item_'+i+'"').should('not.exist')

      cy.get('[data-cy="input_new"]').type(ELEMENTS[i].name)
      cy.get('[data-cy="button_new"]').click()
      cy.get('[data-cy="input_new"]').should('have.value', '')
      cy.get('[data-cy="item_'+i+'"').should('exist')

      cy.get('[data-cy="item_' + i + '"').should('exist')
      cy.get('[data-cy="item_' + i + '_name"').should('contain.text', ELEMENTS[i].name)
      cy.get('[data-cy="item_' + i + '_brand"').should('contain.text', ': ')
      cy.get('[data-cy="item_' + i + '_desc"').should('contain.text', ': ')
      cy.get('[data-cy="item_' + i + '_price"').should('contain.text', '0')
      cy.get('[data-cy="item_' + i + '_inventory_target"').should('contain.text', '0')
      cy.get('[data-cy="item_' + i + '_ingredients"').should('not.exist')
    }

    for(var i=ELEMENTS.length-1;i>=0;i--){
      cy.get('[data-cy="button_' + i + '_delete"').click()
    }
    cy.get('[data-cy="item_0"').should('not.exist')
  })
  it('edit items', () => {
    for(var i=0;i<ELEMENTS.length;i++){
      cy.get('[data-cy="input_new"]').type(ELEMENTS[i].name)
      cy.get('[data-cy="button_new"]').click()
      cy.get('[data-cy="input_new"]').should('have.value', '')
    }

    for(var l=0;l<2;l++){
      for(var i=0;i<ELEMENTS.length;i++){
        //Edit
        cy.get('[data-cy="button_' + i + '_edit"').click()
        cy.get('[data-cy="modal_update"]').should('be.disabled')
        cy.get('[data-cy="input_desc"]').type(ELEMENTS[i].desc)
        cy.get('[data-cy="input_brand"]').type(ELEMENTS[i].brand)
        cy.get('[data-cy="modal_update"]').should('be.disabled')
        for(var j=0;j<ELEMENTS[i].ingredients.length;j++){
          cy.get('[data-cy="add_ingredient"').click()
          cy.get('[data-cy="input_ingredient_' + j + '"').type(ELEMENTS[i].ingredients[j])
        }
        cy.get('[data-cy="input_price"]').type(ELEMENTS[i].price.toString())
        cy.get('[data-cy="modal_update"]').should('be.enabled')
        cy.get('[data-cy="input_inventory_target"]').type(ELEMENTS[i].inventory_target.toString())
        
        //Cancel
        if(l==0){
          cy.get('[data-cy="modal_cancel"]').click()

          cy.get('[data-cy="item_' + i + '_brand"').should('not.contain.text', ELEMENTS[i].brand)
          cy.get('[data-cy="item_' + i + '_desc"').should('not.contain.text', ELEMENTS[i].desc)
          cy.get('[data-cy="item_' + i + '_price"').should('not.contain.text', ELEMENTS[i].price.toString())
          cy.get('[data-cy="item_' + i + '_inventory_target"').should('not.contain.text', ELEMENTS[i].inventory_target)
          cy.get('[data-cy="item_' + i + '_ingredients"').should('not.exist')
        }
        //Update
        else{
          cy.get('[data-cy="modal_update"]').click()

          cy.get('[data-cy="item_' + i + '_brand"').should('contain.text', ELEMENTS[i].brand)
          cy.get('[data-cy="item_' + i + '_desc"').should('contain.text', ELEMENTS[i].desc)
          cy.get('[data-cy="item_' + i + '_price"').should('contain.text', ELEMENTS[i].price.toString())
          cy.get('[data-cy="item_' + i + '_inventory_target"').should('contain.text', ELEMENTS[i].inventory_target)
          for(var j=0;j<ELEMENTS[i].ingredients.length;j++){
            cy.get('[data-cy="ingredient_' + i + '_' + j + '"').should('contain.text', ELEMENTS[i].ingredients[j])
          }
        }
      }
    }
  })
})