const ELEMENTS = [{
  name: "Test1",
  brand: "Brand1",
  desc: "Desc1",
  ingredients: [],
  price: 2.2,
  inventory_target: 1
},
{
  name: "Test2",
  brand: "Brand2",
  desc: "Desc2",
  ingredients: ["Salt"],
  price: 3.1,
  inventory_target: 2
}
];

const LISTS = [
  {
    name: "List1",
    buy_until: "2023-04-12",
    items: [
      {
        type: 1,
        amount: 4
      },
      {
        type: 0,
        amount: 12
      }
    ]
  },
  {
    name: "List2",
    buy_until: "2023-03-01",
    items: [
      {
        type: 0,
        amount: 7
      },
      {
        type: 0,
        amount: 9
      },
      {
        type: 1,
        amount: 2
      }
    ]
  }
]

const LISTS_SORTED = LISTS.sort((n1, n2) => {
  if(n1.buy_until > n2.buy_until){
    return 1;
  }
  if(n1.buy_until < n2.buy_until){
    return -1;
  }
  return 0;
});

beforeEach(() => {
  cy.visit('/')

  for(var i=0;i<ELEMENTS.length;i++){
    cy.get('[data-cy="input_new"]').type(ELEMENTS[i].name)
    cy.get('[data-cy="button_new"]').click()

    cy.get('[data-cy="button_' + i + '_edit"').click()
    cy.get('[data-cy="input_desc"]').type(ELEMENTS[i].desc)
    cy.get('[data-cy="input_brand"]').type(ELEMENTS[i].brand)
    for(var j=0;j<ELEMENTS[i].ingredients.length;j++){
      cy.get('[data-cy="add_ingredient"').click()
      cy.get('[data-cy="input_ingredient_' + j + '"').type(ELEMENTS[i].ingredients[j])
    }
    cy.get('[data-cy="input_price"]').type(ELEMENTS[i].price.toString())
    cy.get('[data-cy="input_inventory_target"]').type(ELEMENTS[i].inventory_target.toString())

    cy.get('[data-cy="modal_update"]').click()
  }

  cy.get('[data-cy="nav_menu_open"]').click()
  cy.get('[data-cy="nav_item_PurchaseListView"]').click()
})

describe('Test purchase lists', () => {
  it('add and remove purchase lists', () => {
    for(var i=0;i<LISTS.length;i++){
      cy.get('[data-cy="input_name"]').clear()
      cy.get('[data-cy="input_buy_until"]').clear()

      cy.get('[data-cy="button_new"]').click();
      cy.get('[data-cy="item_' + i + '"').should('not.exist');

      cy.get('[data-cy="input_name"]').type(LISTS[i].name);

      cy.get('[data-cy="button_new"]').click();
      cy.get('[data-cy="item_' + i + '"').should('not.exist');

      cy.get('[data-cy="input_buy_until"]').type(LISTS[i].buy_until);

      cy.get('[data-cy="button_new"]').click();
      cy.get('[data-cy="item_' + i + '"').should('exist');
    }

    //check list values
    for(var i=0;i<LISTS_SORTED.length;i++){
      cy.get('[data-cy="item_name_' + i + '"').should('contain.text', LISTS_SORTED[i].name);
      cy.get('[data-cy="item_buy_until_' + i + '"').should('contain.text', new Date(LISTS_SORTED[i].buy_until).toLocaleDateString());
    }

    //remove purchase lists
    for(var i=LISTS.length-1;i>=0;i--){
      cy.get('[data-cy="item_rem_' + i + '"').click();
      cy.get('[data-cy="item_' + i + '"').should('not.exist');
    }
  })
  it('edit items with and without saving', () => {
    for(var i=0;i<LISTS.length;i++){
      cy.get('[data-cy="input_name"]').clear();
      cy.get('[data-cy="input_buy_until"]').clear();
      cy.get('[data-cy="input_name"]').type(LISTS[i].name);
      cy.get('[data-cy="input_buy_until"]').type(LISTS[i].buy_until);
      cy.get('[data-cy="button_new"]').click();

      for(var l=0;l<2;l++){
        //edit purchase lists
        cy.get('[data-cy="item_edit_' + i + '"').click();

        for(var j=0;j<LISTS_SORTED[i].items.length;j++){
          cy.get('[data-cy="select_product"]').select(LISTS_SORTED[i].items[j].type);
          cy.get('[data-cy="input_amount"]').clear().type(LISTS_SORTED[i].items[j].amount.toString());
          cy.get('[data-cy="button_add"]').click();
        }

        if(l==0){
          //cancel editing
          cy.get('[data-cy="modal_cancel"]').click();

          cy.get('[data-cy="item_edit_' + i + '"').click();

          for(var j=0;j<LISTS_SORTED[i].items.length;j++){
            cy.get('[data-cy="table_item'+j+'"').should('not.exist');
          }

          cy.get('[data-cy="modal_cancel"]').click();
        }
        else{
          //save editing
          cy.get('[data-cy="modal_update"]').click();

          cy.get('[data-cy="item_edit_' + i + '"').click();

          for(var j=0;j<LISTS_SORTED[i].items.length;j++){
            cy.get('[data-cy="table_product_'+j+'"').should('contain.text', ELEMENTS[LISTS_SORTED[i].items[j].type].name);
            cy.get('[data-cy="table_amount_'+j+'"').should('contain.text', LISTS_SORTED[i].items[j].amount);
          }

          cy.get('[data-cy="modal_cancel"]').click();
        }
      }
    }
  })
})