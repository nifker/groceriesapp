import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import Toaster from '@meforma/vue-toaster'

const app = createApp(App)

app.use(createPinia())

app.use(Toaster)

app.mount('#app')
