import { defineStore } from 'pinia'
import type { ProductType, Product } from '@/classes/product';
import type { PurchaseList } from '@/classes/purchase';

export const productTypeStore = defineStore({
	id: "productTypeStore",
	state: () => ({
		product_types: [] as Array<ProductType>,
		edit: null as ProductType | null,
		addToShoppingList: null as ProductType | null
	}),
	actions: {
		addProductType(product_type: ProductType){
			this.product_types.push(product_type);
		},
		remProductType(product_type: ProductType){
			const index = this.product_types.findIndex(v => v.id == product_type.id);
			if (index > -1) {
				this.product_types.splice(index, 1);
			}
		},
		updateProductType(product_type: ProductType){
			const index = this.product_types.findIndex(v => v.id == product_type.id);
			this.product_types[index] = product_type;
		},
		setEditProductType(product_type: ProductType | null){
			this.edit = product_type;
		},
		setAddToPurchaseList(product_type: ProductType | null){
			this.addToShoppingList = product_type;
		},
		setProductTypes(list: Array<ProductType>){
			this.product_types = list;
		}
	},
	getters: {
		getProductTypes(state): Array<ProductType>{
			return this.product_types;
		},
		getProductTypesValid(state): Array<ProductType>{
			return this.product_types.filter(val => val.isValid());
		},
		getEditProductType(state): ProductType | null{
			return this.edit;
		},
		getAddToPurchaseList(state): ProductType | null{
			return this.addToShoppingList;
		}
	}
})

export const productStore = defineStore({
	id: "productStore",
	state: () => ({
		products: [] as Array<Product>,
	}),
	actions: {
		addProduct(product: Product){
			this.products.push(product);
		},
		remProduct(product: Product){
			const index = this.products.findIndex(v => v.id == product.id);
			if (index > -1) {
				this.products.splice(index, 1);
			}
		},
		remProductType(product_type: ProductType){
			this.products = this.products.filter(val => val.type.id != product_type.id);
		},
		updateProduct(product: Product){
			const index = this.products.findIndex(v => v.id == product.id);
			this.products[index] = product;
		},
		setProducts(list: Array<Product>){
			this.products = list;
		}
	},
	getters: {
		getProducts(state): Array<Product>{
			return this.products;
		},
		getUniqueStorageLocations(state): Set<string>{
			return new Set(this.products.map((val) => val.storage_location))
		}
	}
})

export const purchaseListStore = defineStore({
	id: "purchaseListStore",
	state: () => ({
		purchase_lists: [] as Array<PurchaseList>,
		edit: null as PurchaseList | null
	}),
	actions: {
		addPurchaseList(purchase_list: PurchaseList){
			this.purchase_lists.push(purchase_list);
		},
		remPurchaseList(purchase_list: PurchaseList){
			const index = this.purchase_lists.findIndex(v => v.id == purchase_list.id);
			if (index > -1) {
				this.purchase_lists.splice(index, 1);
			}
		},
		updatePurchaseList(purchase_list: PurchaseList){
			const index = this.purchase_lists.findIndex(v => v.id == purchase_list.id);
			this.purchase_lists[index] = purchase_list;
		},
		setEditPurchaseList(purchase_list: PurchaseList | null){
			this.edit = purchase_list;
		},
		setPurchaseLists(list: Array<PurchaseList>){
			this.purchase_lists = list;
		}
	},
	getters: {
		getPurchaseLists(state): Array<PurchaseList>{
			return this.purchase_lists;
		},
		getEditPurchaseList(state): PurchaseList | null{
			return this.edit;
		}
	}
})