import { ProductType } from "./product";
import { v4 } from 'uuid';



export class ProductAmount{
	public product: ProductType;
	public amount: number;
	public bought: boolean;

	public constructor(product: ProductType, amount: number){
		this.product = product;
		this.amount = amount;
		this.bought = false;
	}
}

export class PurchaseList{
	id: string;
	public name: string;
	public created: Date;
	public buy_until: Date;
	public products: Array<ProductAmount>;

	public constructor(name: string, buy_until: Date){
		this.id = v4();
		this.name = name;
		this.created = new Date();
		this.buy_until = buy_until;
		this.products = new Array();
	}

	public toString(): string{
		return this.name;
	}

	public static fromJSON(json: string): Array<PurchaseList>{
		var object: Array<PurchaseList> = JSON.parse(json);
		var list = [];
		for(var i=0;i<object.length;i++){
			var n: PurchaseList = new PurchaseList(object[i].name, object[i].buy_until);
			n.id = object[i].id;
			n.created = new Date(object[i].created);
			var products = [];
			for(var j=0;j<object[i].products.length;j++){
				var p = ProductType.fromObject(object[i].products[j].product);
				var a = new ProductAmount(p, object[i].products[j].amount);
				a.bought = object[i].products[j].bought;
				products.push(a);
			}
			n.products = products;
			list.push(n);
		}
		return list;
	}
}
