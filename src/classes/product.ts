import { v4 } from 'uuid';

export class ProductType{
	id: string;
	name: string;
	desc: string;
	ingredients: Array<string>;
	price: number;
	brand: string;
	inventory_target: number;

	public constructor(name: string, desc: string, ingredients: Array<string>, price: number, brand: string, inventory_target: number){
		this.id = v4();
		this.name = name;
		this.desc = desc;
		this.ingredients = ingredients;
		this.price = price;
		this.brand = brand;
		this.inventory_target = inventory_target;
	}

	public toString(): string{
		return this.name + " (" + this.brand + ")";
	}

	public isValid(): boolean{
		return this.name.length > 0 && this.price > 0 && this.brand.length > 0;
	}

	public static fromJSON(json: string): Array<ProductType>{
		var object: Array<ProductType> = JSON.parse(json);
		var list = [];
		for(var i=0;i<object.length;i++){
			var n: ProductType = ProductType.fromObject(object[i]);
			list.push(n);
		}
		return list;
	}

	public static fromObject(object: ProductType): ProductType{
		var n: ProductType = new ProductType(object.name, object.desc, object.ingredients, object.price, object.brand, object.inventory_target);
		n.id = object.id;
		return n;
	}
}

export class Product{
	id: string;
	type: ProductType;
	bought_date: Date;
	due_to: Date;
	storage_location: string;

	public constructor(type: ProductType, due_to: Date, storage_location: string){
		this.id = v4();
		this.type = type;
		this.bought_date = new Date();
		this.due_to = due_to;
		this.storage_location = storage_location;
	}

	public static fromJSON(json: string): Array<Product>{
		var object: Array<Product> = JSON.parse(json);
		var list = [];
		for(var i=0;i<object.length;i++){
			var t: ProductType = ProductType.fromObject(object[i].type);
			var n: Product = new Product(t, new Date(object[i].due_to), object[i].storage_location);
			n.bought_date = new Date(object[i].bought_date);
			n.id = object[i].id;
			list.push(n);
		}
		return list;
	}
}
